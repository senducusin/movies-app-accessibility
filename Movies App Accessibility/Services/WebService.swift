//
//  WebService.swift
//  Movies App Accessibility
//
//  Created by Jansen Ducusin on 5/19/21.
//

import Foundation

enum NetworkError: Error {
    case decodeError, urlError, domainError
}

enum HttpMethod: String {
    case post = "POST"
    case get = "GET"
}

struct Resource<T:Decodable> {
    var url: URL
    var httpMethod: HttpMethod = .get
    var httpBody: Data? = nil
}

extension Resource {
    init(url: URL){
        self.url = url
    }
}

class WebService{
    static let shared = WebService()
    static let API_KEY = "65a7aa7e"
    
    func load<T>(resource: Resource<T>, completion:@escaping(Result<T,NetworkError>)->()){
        var request = URLRequest(url: resource.url)
        request.httpMethod = resource.httpMethod.rawValue
        request.httpBody = resource.httpBody
        request.addValue("json/application", forHTTPHeaderField: "Content-Type")
        
        URLSession.shared.dataTask(with: request) { data, _, error in
            guard error == nil,
                  let data = data else {
                completion(.failure(.domainError))
                return
            }
            
            do {
                let result = try JSONDecoder().decode(T.self, from: data)
                completion(.success(result))
            }catch{
                completion(.failure(.decodeError))
            }
            
        }.resume()
    }
}
