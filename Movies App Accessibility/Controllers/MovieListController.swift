//
//  MovieListController.swift
//  Movies App Accessibility
//
//  Created by Jansen Ducusin on 5/19/21.
//

import UIKit

class MovieListController: UITableViewController {
    
    // MARK: - Properties
    var viewModel = MovieListViewModel()
    
    private let searchController = UISearchController(searchResultsController: nil)
    
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
        setupTableView()
        
        viewModel.getAllMovies {
            self.tableView.reloadData()
        }
    }
    
    // MARK: - Helpers
    private func setupTableView(){
        tableView.register(MoviesListCell.self, forCellReuseIdentifier: MoviesListCell.cellIdentifier)
        tableView.rowHeight = 150
        tableView.tableFooterView = UIView()
    }
    
    private func setupUI(){
        title = "Movies"
        navigationController?.navigationBar.prefersLargeTitles = true
        
        setupSearchController()
    }
    
    private func setupSearchController() {
        searchController.searchResultsUpdater = self
        searchController.searchBar.delegate = self
        searchController.searchBar.showsCancelButton = false
        navigationItem.searchController = searchController
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.hidesNavigationBarDuringPresentation = false
        searchController.searchBar.placeholder = "Type a movie title"
        definesPresentationContext = false
    }
}

// MARK: - TableView Delegate and DataSource
extension MovieListController {
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfRows
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: MoviesListCell.cellIdentifier, for: indexPath) as! MoviesListCell
        
        cell.movie = viewModel.movieAt(index: indexPath.row)
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let controller = MovieDetails(movie: viewModel.movieAt(index: indexPath.row))
        navigationController?.pushViewController(controller, animated: true)
    }
}

// MARK: - UISearchResultUpdating
extension MovieListController: UISearchBarDelegate, UISearchResultsUpdating{
    func updateSearchResults(for searchController: UISearchController) {
        guard let keyword = searchController.searchBar.text?.lowercased() else {return}
        
        viewModel.keyword = keyword

    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar){
        viewModel.getAllMovies {
            self.tableView.reloadData()
        }
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar){
        print("DEBUG: cancel")
    }
    
}

