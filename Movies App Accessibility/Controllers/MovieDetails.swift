//
//  MovieDetails.swift
//  Movies App Accessibility
//
//  Created by Jansen Ducusin on 5/19/21.
//

import UIKit

class MovieDetails: UIViewController {
    // MARK: - Properties
    var viewModel: MovieDetailsViewModel!
    
    let contentView: UIView = {
        let view = UIView()
        return view
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.font = .systemFont(ofSize: 24)
        return label
    }()
    
    private lazy var plotLabel: UILabel = {
        let label = UILabel()
        label.font = .systemFont(ofSize: 14)
        label.numberOfLines = 0
        return label
    }()
    
    private lazy var poster: UIImageView = {
        let imageView = UIImageView()
        
        return imageView
    }()
    
    private lazy var scrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.backgroundColor = .white
        scrollView.addSubview(contentView)
        contentView.anchor(
            top:scrollView.topAnchor,
            bottom: scrollView.bottomAnchor,
            width: view.frame.width,
            height: 1000
        )
        
        return scrollView
    }()
    
    // MARK: - Lifecycle
    init(movie: Movie){
        viewModel = MovieDetailsViewModel(movie: movie)
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
        viewModel.getMovieBy{
            self.configure()
        }
        
    }
    
    // MARK: - Helpers
    private func setupUI(){
        view.backgroundColor = .white
        title = viewModel.title
        setupScrollView()
        setupPosterImage()
        setupTitle()
        setupPlot()
    }
    
    private func setupScrollView(){
        view.addSubview(scrollView)
        scrollView.anchor(top: view.safeAreaLayoutGuide.topAnchor, left: view.leftAnchor,  right: view.rightAnchor, height: view.frame.height)
    }
    
    private func setupPosterImage(){
        contentView.addSubview(poster)
        poster.centerX(inView: contentView)
        poster.anchor(
            top: contentView.topAnchor,
            left: view.leftAnchor,
            right: view.rightAnchor,
            paddingTop: 18,
            paddingLeft: 18,
            paddingRight: 18,
            height: 550)
    }
    
    private func setupTitle(){
        contentView.addSubview(titleLabel)
        titleLabel.anchor(top: poster.bottomAnchor, left: view.leftAnchor, paddingTop: 5, paddingLeft: 17)
    }
    
    private func setupPlot(){
        contentView.addSubview(plotLabel)
        plotLabel.anchor(top: titleLabel.bottomAnchor, left: view.leftAnchor, right: view.rightAnchor, paddingTop: 5, paddingLeft: 17, paddingRight: 17)
    }
    
    private func configure(){
        poster.sd_setImage(with: viewModel.posterUrl, placeholderImage: UIImage(systemName: "person.crop.rectangle"))
        titleLabel.text = viewModel.title
        plotLabel.text = viewModel.plot
        
        poster.accessibilityLabel = "\(viewModel.title) poster"
        poster.isAccessibilityElement = true
    }
}
