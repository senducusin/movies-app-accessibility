//
//  MovieDetailsViewModel.swift
//  Movies App Accessibility
//
//  Created by Jansen Ducusin on 5/19/21.
//

import Foundation

class MovieDetailsViewModel {
    var movie: Movie
    
    init(movie: Movie){
        self.movie = movie
    }
    
    var title: String {
        movie.title
    }
    
    var plot: String?{
        movie.plot
    }
    
    var posterUrl: URL?{
        URL(string: movie.poster)
    }
    
    func getMovieBy(completion:@escaping(()->())){
        guard let resource = Movie.getMovieBy(id: movie.imdbId) else {return}
        
        WebService.shared.load(resource: resource) { [weak self] result in
            switch result {
            
            case .success(let movie):
                DispatchQueue.main.async {
                    self?.movie = movie
                    completion()
                }
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
    }
}
