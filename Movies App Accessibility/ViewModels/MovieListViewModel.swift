//
//  MovieListViewModel.swift
//  Movies App Accessibility
//
//  Created by Jansen Ducusin on 5/19/21.
//

import Foundation

class MovieListViewModel {
    
    var movies = [Movie]()
    
    var keyword: String?
    
    var numberOfRows:Int {
        movies.count
    }
    
}

extension MovieListViewModel {
    
    func movieAt(index: Int) -> Movie {
        return movies[index]
    }
    
    func getAllMovies(completion:@escaping(()->())){
        
        guard let keyword = keyword,
              let resource = MovieResponse.getMoviesBy(keyword: keyword) else {return}
        
        WebService.shared.load(resource: resource) { [weak self] result in
            switch result {
            
            case .success(let response):
                DispatchQueue.main.async {
                    self?.movies = response.movies
                    completion()
                }
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
    }
    
    func clearList(completion:@escaping(()->())){
        movies = [Movie]()
    }
}
