//
//  MoviesListCell.swift
//  Movies App Accessibility
//
//  Created by Jansen Ducusin on 5/19/21.
//

import UIKit
import SDWebImage

class MoviesListCell: UITableViewCell {
    // MARK: - Properties
    static let cellIdentifier = "MoviesListCell"
    
    private let poster: UIImageView = {
        let imageView = UIImageView()
        imageView.layer.cornerRadius = 8
        imageView.layer.masksToBounds = true
        
        return imageView
    }()
    
    private let title: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.font = .boldSystemFont(ofSize: 16)
        
        return label
    }()
    
    private let year: UILabel = {
        let label = UILabel()
        label.font = .systemFont(ofSize: 16)
        label.textColor = .darkGray
        
        return label
    }()
    
    var movie: Movie? {
        didSet {
            configure()
        }
    }
    
    // MARK: - Lifecycle
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?){
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        setupUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Helpers
    private func setupUI(){
        accessoryType = .disclosureIndicator
        
        setupPoster()
        setupTitle()
        setupYear()
    }
    
    private func setupPoster(){
        addSubview(poster)
        poster.anchor(top: topAnchor, left: leftAnchor, bottom: bottomAnchor, paddingTop: 5, paddingLeft: 17, paddingBottom: 5, width: 100)
    }
    
    private func setupTitle(){
        addSubview(title)
        title.anchor(top:topAnchor, left: poster.rightAnchor, right: rightAnchor, paddingTop: 5, paddingLeft: 10, paddingRight: 50)
    }
    
    private func setupYear(){
        addSubview(year)
        year.anchor(top: title.bottomAnchor, left: poster.rightAnchor, paddingTop: 20, paddingLeft: 10)
    }
    
    private func configure(){
        guard let movie = movie else {return}
        
        title.text = movie.title
        title.accessibilityLabel = movie.title
        
        year.text = movie.year
        year.accessibilityLabel = movie.year
        
        poster.sd_setImage(with: URL(string: movie.poster), placeholderImage:#imageLiteral(resourceName: "placeholder"))
    }
}
