//
//  Movie.swift
//  Movies App Accessibility
//
//  Created by Jansen Ducusin on 5/19/21.
//

import Foundation

struct Movie: Codable {
    let title: String
    let year: String
    let imdbId: String
    let poster: String
    let plot: String?
    let imdbRating: String?
    let director: String?

    private enum CodingKeys: String, CodingKey {
        case title = "Title"
        case year = "Year"
        case imdbId = "imdbID"
        case poster = "Poster"
        case plot = "Plot"
        case imdbRating = "imdbRating"
        case director = "Director"
    }
}

extension Movie {
    static func getMovieBy(id: String) -> Resource<Movie>?{
        guard let url = URL(string: "http://omdbapi.com/?i=\(id)&apikey=\(WebService.API_KEY)") else {return nil}
        
        return Resource(url: url)
    }
}
